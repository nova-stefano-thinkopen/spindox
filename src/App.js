import React, { Component } from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser, faEnvelope, faCalendarAlt, faMapMarked, faPhoneAlt, faLock } from '@fortawesome/free-solid-svg-icons';

import Stack from 'react-bootstrap/Stack';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import Image from 'react-bootstrap/Image';

import 'bootstrap/dist/css/bootstrap.min.css';

import './App.css';

class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      isLoaded: false,
      ajaxResponse: null
    }
  }

  componentDidMount() {
    fetch('https://randomuser.me/api')
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            title: null,
            subtitle: null,
            ajaxResponse: result.results[0]
          });
        },
        (error) => {
          console.log(error);
          this.setState({ isLoaded: true });
        }
      )
  }

  clear() {
    this.setState({ title: null, subtitle: null })
  }

  set(title, subtitle) {
    this.setState({ title: title, subtitle: subtitle })
  }

  render() {
    return (
      <div className="App">
        {this.state.isLoaded &&
          <Container>
            <Stack gap={5}>
              <Row>
                <Col>
                  <Image src={this.state.ajaxResponse.picture.large} roundedCircle />
                </Col>
              </Row>
              <Row style={{height: '30px'}}>
                <Col style={{fontSize: '1rem'}}>
                  <div>{this.state.title}</div>
                </Col>
              </Row>
              <Row style={{height: '40px'}}>
                <Col style={{fontSize: '1.5rem'}}>
                  <div>{this.state.subtitle}</div>
                </Col>
              </Row>
              <Row>
                <Col
                className="style-over"
                onMouseOut={() => { this.clear() }}
                onMouseOver={() => { this.set('Hi, My name is', this.state.ajaxResponse.name.first + ' ' + this.state.ajaxResponse.name.last) }}>
                  <FontAwesomeIcon icon={faUser} />
                </Col>
                <Col
                className="style-over"
                onMouseOut={() => { this.clear() }}
                onMouseOver={() => { this.set('My email address is', this.state.ajaxResponse.email) }}>
                  <FontAwesomeIcon icon={faEnvelope} />
                </Col>
                <Col
                className="style-over"
                onMouseOut={() => { this.clear() }}
                onMouseOver={() => { this.set('My birthday is', new Date(this.state.ajaxResponse.dob.date).toLocaleDateString()) }}>
                  <FontAwesomeIcon icon={faCalendarAlt} />
                </Col>
                <Col
                className="style-over"
                onMouseOut={() => { this.clear() }}
                onMouseOver={() => { this.set('I live in', this.state.ajaxResponse.location.country) }}>
                  <FontAwesomeIcon icon={faMapMarked} />
                </Col>
                <Col
                className="style-over"
                onMouseOut={() => { this.clear() }}
                onMouseOver={() => { this.set('My phone number is', this.state.ajaxResponse.phone) }}>
                  <FontAwesomeIcon icon={faPhoneAlt} />
                </Col>
                <Col
                className="style-over"
                onMouseOut={() => { this.clear() }}
                onMouseOver={() => { this.set('My password is', this.state.ajaxResponse.login.password) }}>
                  <FontAwesomeIcon icon={faLock} />
                </Col>
              </Row>
            </Stack>
          </Container>
        }
      </div>
    );
  }
}

export default App;
